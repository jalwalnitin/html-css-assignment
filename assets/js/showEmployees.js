const showEmployees = (function(){
    const displayDetails = () => {
        let empDetails = JSON.parse(localStorage.getItem("employeeDatabase") || "[]");
    
        empDetails.forEach((element, index) => {
            addRow(element, index);
        });
    };
    
    const addRow = (data, index) => {
        const table = document.getElementById("empTable");
        const row = table.insertRow();
    
        row.onclick = showOnMap.bind(row, data.latitude, data.longitude);
        const indexCell = row.insertCell();
        indexCell.innerHTML = index + 1;
        indexCell.setAttribute("scope", "row");
        const name = row.insertCell();
        const phoneNo = row.insertCell();
        const address = row.insertCell();
        const email = row.insertCell();
    
        name.innerHTML = data.name;
        phoneNo.innerHTML = data.contact;
        email.innerHTML = data.email;
        address.innerHTML = data.address;
    };

    const showOnMap = (latitude, longitude) => {
        const position = { lat: Number(latitude), lng: Number(longitude) };

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 8,
            center: position,
        });
        const marker = new google.maps.Marker({
            position: position,
            map: map,
          });
    };

    return {
        show: () => {
            displayDetails();
        }
    }
})();


document.body.onload =() => {
    showEmployees.show();
}

