import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";

const form = document.getElementById('addEmployee');

form.addEventListener('submit', (event) => {
    const result = {
        name: form.elements["employeeName"].value,
        email: form.elements["employeeEmail"].value,
        contact: form.elements["employeeContact"].value,
        address: form.elements["employeeAddress"].value,
        latitude: form.elements["latitude"].value,
        longitude: form.elements["longitude"].value,
    }

    let empDatabase = localStorage.getItem("employeeDatabase") || "[]";
    empDatabase = JSON.parse(empDatabase);

    empDatabase.push(result);

    localStorage.setItem("employeeDatabase", JSON.stringify(empDatabase));
});